#!/usr/bin/env bash
rm -r dist/webfonts
cp -r ./node_modules/@fortawesome/fontawesome-free/webfonts/ dist/webfonts
rm dist/webfonts/*.eot
ls -ahl dist/webfonts 
echo "#!/usr/bin/fontforge" > reduce.pe
echo 'i=1' >> reduce.pe
echo 'while(i< $argc)' >> reduce.pe
echo 'file= $argv[i]' >> reduce.pe
echo "Open(file)" >> reduce.pe
grep -oE "\.fa-[^[:space:]{})(.,:]+" dist/assets/*.css|sort| uniq|cut -c 5-|awk '{print "SelectMoreIf(\"" $1 "\")"}' >> reduce.pe
echo "SelectInvert()" >>reduce.pe
echo "Clear()" >> reduce.pe
echo "Generate(file)" >> reduce.pe
echo "i++" >> reduce.pe
echo "endloop" >> reduce.pe

chmod +x reduce.pe
./reduce.pe dist/webfonts/*
rm ./reduce.pe
