import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import PurgeSvelte from "purgecss-from-svelte";
import purgecss from "@fullhuman/postcss-purgecss";
const options = {
  content: ["./src/**/*.svelte", "./index.html" ],
  extractors: [
    {
      extractor: (content) => PurgeSvelte.extract(content),
      extensions: ["svelte"],
    },
  ],
  safelist: [/svelte-/, /data-tooltip/ ],
};
const isProduction = process.env.NODE_ENV === "production";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte()],
  resolve:{
    alias:{
      "$src":"/src",
      "$lib":"/src/lib",
      "$assets":"/src/assets",
    }
  },
  css: {
    postcss: {
      plugins: isProduction ? [purgecss(options)] : [],
    },
  },
});
