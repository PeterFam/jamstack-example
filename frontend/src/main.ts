import App from './App.svelte'
import "$assets/styles/theme.scss"

const app = new App({
  target: document.body
})

export default app
