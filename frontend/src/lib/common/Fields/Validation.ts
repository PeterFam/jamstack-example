import { is, string, pattern } from "superstruct";
export enum FieldState {
  isInitial,
  isEmpty,
  isValid,
  isError,
  isValidationDisabled,
}
const email = pattern(
  string(),
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
const password = pattern(
  string(),
  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/
);
export const validateEmail = (value: string) => is(value, email);
export const validateUsername = (value: string) =>
  is(value, pattern(string(), /^[a-z0-9_-]{3,15}$/));
export const validatePassword = (value: string) => is(value, password);
export const isValidField = (state: FieldState) =>
  state === FieldState.isValid || state === FieldState.isValidationDisabled;

export const validateForm = (
  data: any,
  states: Array<FieldState>,
  customValidate = (_: any) => true
) => {
  return customValidate(data) && states.every(isValidField);
};
