let ENDPOINT = "";
if (window.location.href.indexOf("localhost") >= 0) {
  ENDPOINT = "http://localhost:1337/graphql";
} else {
  ENDPOINT = "backend/graphql";
}
export const GRAPHQL_ENDPOINT = ENDPOINT;
