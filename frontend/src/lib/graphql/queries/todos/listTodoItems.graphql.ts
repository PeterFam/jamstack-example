export default `
query listTodoItems($user:ID!){
		todoItems(where:{user:$user}){
    id
    description
    checked
    created_at
  }
}
`
