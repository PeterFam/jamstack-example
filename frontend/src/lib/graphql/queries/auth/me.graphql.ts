export default `
  {
    me {
      id
      username
      email
      confirmed
    }
  }
`
