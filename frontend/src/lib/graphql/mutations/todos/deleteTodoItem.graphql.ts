export default `
mutation deleteTodoItem($id:ID!){
  deleteTodoItem(input:{where:{id:$id}}){
    todoItem {
      id
    }
  }
}
`
