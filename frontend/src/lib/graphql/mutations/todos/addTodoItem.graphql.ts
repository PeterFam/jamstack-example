export default `
mutation addTodoItem($description:String!, $checked:Boolean!,$user:ID!){
  createTodoItem(input:{data:{description:$description,checked:$checked,user:$user}}){
    todoItem{
      description
      id
      checked
      created_at
    }
  }
}
`
