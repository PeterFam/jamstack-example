export default `
mutation updateTodoItem($id:ID!,$description:String,$checked:Boolean){
	updateTodoItem(input:{data:{
    description:$description,
    checked:$checked
  },where:{id:$id}}){
    todoItem{
      id
      description
      checked
    }
  }
}
`
