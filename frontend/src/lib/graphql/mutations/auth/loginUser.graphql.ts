export default `
mutation LoginUser($identifier: String!, $password: String!) {
  login(input: { identifier: $identifier, password: $password }) {
    jwt
    user {
      id
      email
      username
      confirmed
    }
  }
}
`;
