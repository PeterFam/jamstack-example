export default `
mutation registerUser(
  $username: String!
    $email: String!
    $password: String!
  ) {
    register(
      input: { username: $username, password: $password, email: $email }
    ) {
      jwt
      user {
        id
        username
        email
        confirmed
      }
    }
  }
`
