import graphql from "graphql.js";
import { get } from "svelte/store";
import { jwt } from "$lib/stores/auth";
import { GRAPHQL_ENDPOINT } from "$lib/graphql/config";
export default graphql(GRAPHQL_ENDPOINT, {
  method: "POST",
  headers: {
    Authorization: () => {
      const token = get(jwt);
      return token ? `Bearer ${token}` : "";
    },
  },
});
