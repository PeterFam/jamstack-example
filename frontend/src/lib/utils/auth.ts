import { jwt, currentUser } from "$lib/stores/auth";
import graph from "$lib/graphql/graph.ts";
import registerUserQuery from "$lib/graphql/mutations/auth/registerUser.graphql";
import loginUserQuery from "$lib/graphql/mutations/auth/loginUser.graphql";

export const logout = () => {
  currentUser.set(null);
  jwt.set("");
};

export const login = async (identifier: string, password: string) => {
  logout();
  return graph(loginUserQuery)({ identifier, password }).then(async (data) => {
    let _jwt = data?.login?.jwt;
    jwt.set(_jwt);
    let _user = data?.login?.user;
    currentUser.set(_user);
    return Promise.resolve(_user);
  });
};

export const signup = async (
  username: string,
  email: string,
  password: string
) => {
  logout();
  return graph(registerUserQuery)({ username, email, password }).then(
    async (data) => {
      let _jwt = data?.register?.jwt;
      jwt.set(_jwt);
      let _user = data?.register?.user;
      currentUser.set(_user);
      return Promise.resolve(_user);
    }
  );
};
