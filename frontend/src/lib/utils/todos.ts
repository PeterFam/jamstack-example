import { get } from "svelte/store";
import { currentUser } from "$lib/stores/auth";
import graph from "$lib/graphql/graph";
import listTodoItemsQuery from "$lib/graphql/queries/todos/listTodoItems.graphql.ts";
import addTodoItemMutation from "$lib/graphql/mutations/todos/addTodoItem.graphql.ts";
import deleteTodoItemMutation from "$lib/graphql/mutations/todos/deleteTodoItem.graphql.ts";
import updateTodoItemMutation from "$lib/graphql/mutations/todos/updateTodoItem.graphql.ts";

export const addTodoItem = (description: string, checked: boolean) => {
  let user = get(currentUser).id as number;
  return graph(addTodoItemMutation)({ user, description, checked });
};

export const updateTodoItem = (
  id: number,
  description: string,
  checked: boolean
) => {
  return graph(updateTodoItemMutation)({ id, description, checked });
};

export const deleteTodoItem = (id: number) => {
  return graph(deleteTodoItemMutation)({ id });
};

export const listTodoItems = () => {
  let user = get(currentUser).id as number;
  return graph(listTodoItemsQuery)({ user });
};
