import { writable, derived } from "svelte/store";
const CURRENTUSER_KEY = "currentUser";
const JWT_KEY = "JWT";

export const currentUser = writable(null, (set: Function) => {
  let _user = sessionStorage.getItem(CURRENTUSER_KEY);
  set(JSON.parse(_user) ?? null);
});
currentUser.subscribe((user: Object) => {
  if (user) {
    sessionStorage.setItem(CURRENTUSER_KEY, JSON.stringify(user));
  } else {
    sessionStorage.removeItem(CURRENTUSER_KEY);
  }
});

export const jwt = writable("", (set: Function) => {
  let _jwt = sessionStorage.getItem(JWT_KEY);
  set(_jwt ?? "");
});
jwt.subscribe((value: string) => {
  if (value) {
    sessionStorage.setItem(JWT_KEY, value);
  } else {
    sessionStorage.removeItem(JWT_KEY);
  }
});

export const isAuthenticated = derived(jwt, ($jwt: string) => {
  if ($jwt) {
    return true;
  }
  return false;
});
